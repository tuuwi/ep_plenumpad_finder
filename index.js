exports.expressConfigure = function (hook, context) {
  var app = context.app;

  app.get(["/plenum", "/p/plenum"], function (req, res) {
    var current = new Date();
    var weekstart = current.getDate() - current.getDay() + 2;
    var newDate = new Date(current.setDate(weekstart));
    var month = newDate.getMonth() + 1;
    var monthString = "";
    if (month < 10) {
      monthString = "0" + month.toString();
    } else {
      monthString = month.toString();
    }
    var dateString = "";
    if (newDate.getDate() < 10) {
      dateString = "0" + newDate.getDate().toString();
    } else {
      dateString = newDate.getDate().toString();
    }
    res.redirect(
      "/p/" +
        newDate.getFullYear().toString() +
        monthString +
        dateString +
        "_tuuwi_plenum"
    );
  });
};
